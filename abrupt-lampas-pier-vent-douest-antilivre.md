---
author: "Pier Lampás"
title: "Vent d'ouest"
publisher: "Abrüpt"
date: 2020
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0119-4'
rights: "© 2020 Abrüpt, CC BY-NC-SA"
adresse: "https://abrupt.cc/pier-lampas/vent-douest"
---

*à Marseille*

*sur le chantier de requalification urbaine*

*de la Place Jean Jaurès*

---

*Ô farouche vent d'ouest, toi souffle de l'être de l'automne, toi dont l'invisible présence chasse les feuilles mortes comme des spectres fuyant devant un enchanteur, jaunes, et noires, et pâles, et d'un rouge de fièvre, multitudes frappées de la peste !* --- Percy Bisshe Shelley.

---

*Les immeubles s'effondrent et c'est la faute du vent.*


# I


Nous n'irons ni à Côme ni à Berlin-Ouest,

Un lac, pour se noyer, ce n'est pas la panacée,

Et je n'ai pas du tout le goût de la musique sèche,

Bien que cela nous permette le voyage à moindres frais,

Nous n'irons pas du tout à Côme ou à Berlin-Ouest.

---

Nous n'irons plus où le rêve, défigure les voies nouvelles,

Mais puisque tu n'es plus là, ami, et que Brecht est parti,

J'irai tout seul casser les siècles et les récits,

En espérant me perdre dans l'ADN d'un mort,

Dans le vieux manifeste d'un nietzschéen de gauche,

Dans le surréalisme de poche de mes amis de Paris,

Dans toute cette crasse, il y a bien de quoi se maudire,

Et toute l'Histoire à travestir, on en connaît le prix.

---

Nous nous efforcerons d'y croire

À ta dialectique, camarade,

Mais dans cette drôle d'apocalypse,

Ton théâtre est fermé,

Le mistral souffle, il fait tout noir,

Je ne sais pas comment

Mettre en rapport les éléments,

Et je me dis que, décidément,

Il ne nous reste rien, ou pas grand-chose,

Qu'appartenir aux vents,

Aux cycles indifférents du temps,

À l'écume et aux vagues,

Aux ressacs,

À l'effondrement des siècles.

---

Nous nous efforcerons d'y croire, pour ne pas finir tout seuls,

Et nous graverons sur un vieux zinc, un bout de plastique affreux,

Le sourire effronté de l'amitié, ce matelot de l'encre verte mâchant

Son bout de sèche, avec des yeux d'ivoire et de ténèbres.

---

Seul, devant les portes de ton théâtre fermé,

Je te promets de rester raisonnable, j'irai jeter Nerval

Et toute la vieille poésie, je ne recommencerai pas

Le cycle des saisons malades, un vieux luth abîmé et tous les soleils noirs,

Et tous les golfes de Naples et tous les volcans et tous les exotismes

S'écraseront contre l'atmosphère infectée de Bagnoli et de Fuorigrotta

--- vieux quartiers ouvriers, au pied du Pausilippe.

---

La Plaine barricadée est un sacré bordel,

Les tambours dissonants n'auront jamais raison,

La guérilla, mon ami, ce n'est pas vraiment ça,

Les conditions objectives ne sont pas réunies,

--- Et puis, tu le connais, toi, le maniement des fusils ?

Pasolini avait raison contre Moravia,

Il faut d'abord apprendre à se parler, s'organiser,

Mais, laisse-moi une dernière fois, promis,

À ses histoires de beatniks dégénérés,

Les tambours dissonants, ce soir, ne réveilleront personne,

Et sa peau, camarade, son épiderme est chaud,

C'est un grand feu de Pâque,

        une économie de braise et de papier.

---

Quand j'étais communiste,

J'étais plus heureux que dans ses bras,

Mais j'ai choisi ses bras et depuis

J'erre aux quatre coins du même bar,

J'ai fait huit fois le tour du monde en restant sur ma chaise,

Je me suis noyé cent fois dans la même mauvaise bière,

Et cette même mauvaise bière me fut plus familière que tous les lacs d'Italie et de Russie,

Et ses délires de punk à chien me furent plus enchanteurs que toutes les dialectiques,

Je ne regrette pas, d'avoir choisi ses bras.

---

Je ne sais quel ravin quelle rivière,

Quelle immensité noire ont accueilli nos *chants*,

À nous en défaire la mâchoire, à nous en démonter l'avant-bras,

À tous nos ricochets, dans les rivières salées du Gard ou de Lozère,

Je préfère me tordre le poignet, me broyer l'humérus, me fracasser l'omoplate

Que de choisir le monde des grands, je n'ai plus peur du soir.

---

Nous nous sommes trompés de rendez-vous je crois

Cette vie n'en vaut pas du tout la peine

C'est très cher et il y a des touristes partout

Reste en enfance c'est bien mieux

Ne grandis jamais tu verras

Comme c'est beau nos larmes amères

Fleurissent déjà

Nos balles fusent et font renaître

--- une douceur perdue.

# II

*La diritta via era smarrita*

---


Qu'importe.

Nous n'étions pas armés pour la grand-route balisée du Savoir.

Nos émerveillements accouchent toujours d'une épopée.

Et ici, il n'y a plus rien à faire.

Éviter de se morfondre, déshydraté, en son désert.

Cette grande Nuit de l'Histoire n'est pas l'Histoire en suspens : elle
est l'Histoire elle-même.

Lutter contre l'oubli. En obscurcir nos raisons. Négliger nos mystères.

Cette absence de faculté pour les arts de la négociation, nous a valu ce
qu'il en coûte habituellement aux insurgés de la lumière : nous avons
épuisé nos rayons, à n'en savoir que faire.

Quand les fards de la marchandise ont installé leurs assises au cœur de
nos boîtes crâniennes. Aucun moyen d'échapper à cette idée maudite de la
gloire et du destin. Une trace de nous, peut-être. Et cette idée de la
grandeur défaite.

Nous nous jetons dans les luttes afin de donner corps et style à la
saison des naufrages : attisant le chaos, nous chérissons le geste.

Mais le pire : nous n'avons jamais cessé d'y croire.

Et pourtant. Nous sommes les dégradés de l'Histoire officielle.
Abandonnés par le mouvement ouvrier international, livrés  à leur Nuit
mesquine et étoilée, nous nous sommes longtemps interdit de croire --
que notre Histoire était terminée.

Et j'ajoute --- en un souci de précision caractérisant tous les amputés
du chant --- comme si la raison était de velours, et non de terre sèche :
que c'est peut-être ainsi que tout devait commencer.

Mais ce n'est pas pour demain. Valeureux passeurs que nous sommes.
Alors, aujourd'hui, n'en déplaise à cette poétique difficile du
quotidien pour laquelle je n'ai guère de grand talent : je pars.

L'automne est triste à Marseille. Les bars déversent leur breuvage
anesthésiant dans les crânes. Artères bouchées par la crasse. Et la
domestication de nos grimaces.

Orbites défoncées, vitalité déclinante de l'aube, logistique de nos
émois : les barricades sont un folklore. Insignifiance flasque de nos
prises d'armes. La lame éreintée du combat. Et nos querelles, dont nous
sommes tous bien las.

Avec ou contre la CGT ? Allons aux Terrasses du port, au Centre bourse,
au carrefour des désolations, un peu de sève et de félicité, nourrir le
bitume, revenir au bar ; l'art de perdre, dit-on.

Nous finirons tous moines, contempteurs naïfs de nos disgrâces, brigands
d'eau douce, charlatans, fosses septiques... Tous unis sous l'étendard
de l'impuissance : nos errances se jaugent sans se compromettre.

Il n'est pas étonnant que nous ayons tant de mal à convaincre, suffit de
voir nos gueules.

Les salariés du Centre bourse, les employées des Terrasses, et ce
carrefour des désolations, tout le monde s'en fout de nos révolutions.

Nous sommes seuls.

Les tenanciers ne nous font pas crédit : ils nous regardent vieillir.
On ne fait crédit qu'aux morts : aux secrétaires de section du Parti
communiste, aux trotskystes, aux peintres du dimanche.

Et nos rixes, quoi qu'on en dise, sont des rivières de jouvence : nous
sommes restés vivants. Bifurcations et magouilles. Travailler le moins
possible. Éviter les contrôles de la CAF. Nous nous sommes armés
d'astuce. Des seigneurs du bitume. Des maîtres sans disciples.
Trafiquants et parjures : nous tenons la Place.

Au prochain printemps, me dit le camarade, une barbe engoncée dans
la nuit, une chevelure de diable, un air de vieux soufi.

Tiens, c'est étrange, on dirait qu'il n'est pas déjà soûl. Il va encore
nous ramollir la nuit avec ses incartades bordiguistes. À moins qu'il ne
nous parle d'amour. Ce qui serait sans aucun doute plus réjouissant, et
plus triste.

L'une veut danser. Contre les fantômes, contre les saisons. L'autre
veut boire, surtout. Il en faut des remèdes, n'est-ce pas, à la
morte-saison. Lui ne viendra pas : il domestique ses peurs en
enfumant ses nuits.

Tu le sais bien, pourtant, que je ne danse qu'au printemps. Mais pour ce
qui est de boire : je ne dis jamais non.

Un camarade propose de finir son dernier litron de vin toscan --- cela
nous fera voyager : il nous raconte Florence, sa brume et ses poisons ;
il nous raconte l'époque, toutes les forêts obscures et nos
bifurcations. Indéniablement, Dante nous sera plus utile qu'Amadeo
Bordiga et sa doctrine de l'invariance de la théorie
marxiste-léniniste : c'est une connerie de croire que
le *cammin* existe, de toute éternité, qu'il nous suffit de le marteler.

*Caminos*, Machado ! Ta simple évocation suffit à réveiller Lorca. Et
nous chantons pour cette Andalousie, du poème, de l'Histoire. Aux
grands départs aussi. Et à ce goût d'*albicocca* que tu trimballes,
de Calabre en Sicile, de Grenade en dépouille.

À cette pluie d'épices que la nuit recrache, alentour, comme un foyer
pour nos rêves invincibles. À l'éternelle enfance, surtout. À laquelle
nous parviendrons. À laquelle nous parvenons déjà.

Descente de flics. Un groupe de jeunes sénégalais se fait contrôler.
Trois anarchistes interviennent généreusement. L'un d'eux se fait
embarquer. La nuit, lourde, reprend ses droits. L'Andalousie n'existe
plus que dans le poème. Mais il ne tient qu'à nous, n'est-ce pas ?

Notre joyeux bordel a cessé de crépiter. On ne surplombe plus rien. Pas
même la nuit. Sans le courage de travestir nos banalités, délicatesse du
style... etc.

Abandonné ce qui, il y a quelques heures encore, nous donnait une raison
de continuer la danse : au détour d'une manif, d'un café, d'une doctrine
de l'invariance de la théorie marxiste-léniniste, d'un trottoir gavé de
poisse et de cris... Au détour, surtout, sans cesse grappillé, au sein
duquel, dans une ruelle, outillés de bière et de poème, nous couchions
quelques instants, avant de prendre plaisir à le voir se déployer,
virevoltant, le vent taquin et inspiré de l'amitié.

Je reviendrai, c'est promis ; chargé des premières lueurs d'un hiver
partisan. Nous écumerons ainsi la saison des outrages. Et nos faillites,
revigorées par l'outrance, danseront comme des soleils de pourpre sur
les chantiers d'Euromed et de la Place Jean Jaurès. Car nous n'aurons
jamais l'âge d'éclore dans un sous-sol d'Éternité. Quand les couchants
ont perdu leur audace. Et les baisers.

Cette grande Nuit de l'Histoire n'est pas l'Histoire en suspens --- elle
est l'Histoire elle-même : ruses, difformités, sainteté dégoulinante,
odes à la pureté, respirations profanes, culs-de-sac.

Nous dansons sur un fil.

Nous, parasites, gueules d'ange hirsutes, increvables bagnards et des
soleils blessés ;

Nous, dépravés ;

Nous, filles et fils maudits de leur grand rêve fracturé ;

Nous, orphelins des révolutions, orphelins d'un ancrage, d'un ciel, d'un
foyer ;

Nous, acrobates, grimaçants et superbes ;

Nous, qui excellons dans l'injure autant que dans la danse ;

Nous habiterons vaillamment ces territoires dépeuplés : que continue de
croître la rose au milieu des ruines.

Ainsi que le figuier sauvage tissant des jeux d'ombre et de lumière sur
un mur décrépi, dans une ruelle sans vie, ayant pris racine au sein d'un
vieux tuyau de canalisation rouillé : nous sommes la nature reprenant
ses droits.

Les fresques vieillies que parcourent les rayons outranciers du jour.

Les nuits fauves.

Et le parfum des méduses.

La plainte béate du souvenir.

Et l'ardeur au combat.

Nous sommes l'Histoire, incertaine et brouillonne, ses effluves, ses
rancœurs, ses distorsions et ses douleurs.

Apôtres de la mauvaise nouveauté, nous chérirons les traditions
vivantes.

Et nous ferons corps avec tous les exilés, les bannis, les opprimés...

Notre route distordue aura la générosité d'un paysage : berceau fertile
d'un peuple qui s'ignore.

*La diritta via era smarrita* --- et c'est sans doute ainsi que notre
histoire doit commencer.

# III

On se fout bien des auteurs, on travestit nos noms,

Tout ce qui importe est de ne pas rester seul,

Dans une vieille C15 qui vrombit aux abords des 80 km/h,

On boit du Cucullia, le cousin pauvre du Pic Saint Loup,

À l'arrière de cette C15 toute blanche et toute rouillée,

On fait semblant de craindre dégun quand on dépasse les 80 km/h,

Nous étions prisonniers d'une façon de faire des vers,

Associant la Beauté à la saison des catastrophes,

Les grands éclats avaient pour nous beaucoup de volupté,

Et puis on s'est lassé,

On a assis la Beauté sur les rebords d'une autoroute,

Et on a fui à plus de 80 km/h sans trembler,

Inventer des voies nouvelles, tu sais,

Il n'y a rien de plus naïf que de renier la technique,

Non loin de Montpellier, le ciel récite ses gammes,

Une petite lucarne ouverte sur le ciel héraultais,

Aux abords de Montpellier, beaucoup d'embouteillages,

Elle m'apprenait le nom des fleuves et de ses affluents,

Nous nous les récitions pour passer le temps,

C'était notre façon de nous parler d'amour,

Demain je ne veux pas rentrer, je ne veux pas travailler,

En 2020 la Canebière sera piétonne, c'est écrit sur des publicités,

Je me demande si en 2020 les délogés seront relogés,

Si les immeubles continueront de s'effondrer,

Je ne sais où nous irons demain,

De Nîmes à Montpellier, c'est un véritable far west,

Mais alors pourquoi ne pas remplacer la C15 par une monture de qualité et mes vers par des balles de revolver ?

Le lundi soir à Sète, ça pue l'urine et l'eau salée,

D'ailleurs, Paul Valery a demandé à être rapatrié,

Tous les junkies de la banlieue sétoise ont pissé sur sa tombe,

Il se satisfait très mal de cette odeur d'urine, l'air salin lui monte à la tête,

Comme Éluard et son air contrit devant des ouvriers récitant ses vers,

Ils n'ont pas l'air heureux que nous les manipulions à notre guise,

D'ailleurs, ils ont tenu une AG exceptionnelle et ont décrété que dans l'ensemble nous avions théoriquement tort,

Et Georges qui se marre au fond du bar, les théories esthétiques ça n'est vraiment pas son fort,

Mais Georges, lui, me comprend très bien,

Il comprend très bien que tous les junkies de la banlieue sétoise aient pissé sur sa tombe,

Nous soignons nos hommages,

Je ne sais vraiment pas ce que je fous seul à Sète un lundi soir d'octobre,

Tout ceci pour une histoire de chat noir,

À Saint Jean de Cuculle, le Cucullia ça vient de là,

Une histoire de chat noir qui nous a toisés longtemps,

Je le considérais comme un frère ce chat noir,

Je l'ai accueilli sur mes cuisses et l'ai blotti contre moi,

Nous étions alors tous les trois assis sur les marches d'une église,

Nous en étions déjà à la seconde bouteille je crois,

Puis le chat noir est monté sur sa jambe et ils ont joué ensemble,

Et je me suis retrouvé tout seul comme un con, ou comme un vieux cow-boy,

C'est à cet instant je crois que j'ai pensé à John Wayne,

J'étais jaloux de ce chat noir que j'ai alors cessé de considérer comme un frère,

Quand on se sent seul il est normal de se donner des airs,

Le Cucullia aidant j'ai dégainé ma fièvre,

Depuis, je suis seul à Sète et je m'emmerde,

Elle vient tout juste de m'écrire,

Son message est atrocement sobre et moi je suis encore ivre,

La sobriété est une impolitesse quand on aime,

C'est ridicule comme nom le Cucullia, surtout quand on est seul,

Ça n'aide vraiment pas à se donner des airs,

Mais les voies nouvelles impliquent, je crois,

De se foutre éperdument des airs que l'on se donne,

Je ne vais quand même pas inventer que je bois du whisky irlandais,

Et puis John Wayne, lui, ne s'est jamais battu contre un chat,

Allez, j'enfourche un TER et je rentre à Marseille,

Ce soir, je n'irai pas voir les copains, pardonnez-moi,

Je m'acclimate très mal aux plaines sans héroïsme,

Allons sur le Vieux-Port nous frotter aux enseignes vulgaires,

Je commande un verre de vin sur le Cours Estienne d'Orves,

Il coûte 4,50\ € et ça me désespère,

Ne comptez plus sur moi pour bâtir un matérialisme vulgaire,

J'aimerais revoir la fine couche du ciel héraultais,

Nos pointes à plus de 80 km/h sur la N113,

Et ce vieux nom qui me rappelle, une façon nouvelle de faire des vers,

Il est bien difficile, pourtant, de restituer les angles morts de mon far west,

Il faut soulever la poussière pour en révéler la teneur,

Je ne vous parle pas de la Beauté éternelle, ce chien crevé,

Nous l'avons abandonné sur les rebords d'une autoroute,

Mais de la compagnie de ce Gitan et de sa "p'tite",

C'est ainsi qu'il l'appelait, de la poudre plein les naseaux,

Ils jouaient à se débattre comme deux amants inquiets,

Ce n'est pas comme ici, sur le Cours Estienne d'Orves, où je suis entouré d'abrutis,

En plus, il y a plein de moucherons dans mon verre,

Et je me mets à rêver que ces moucherons virevoltants déversent du poison dans tous les verres de tous ces abrutis buvant des spritz à 8\ € sur le Cours Estienne d'Orves,

Parfois j'ai des accès de colère et je me mets à rêver,

Mais je n'ai jamais fait de mal à personne, c'est juste un rêve,

Je regarde discrètement la cuisse gauche de mon voisin,

J'ai beaucoup de tendresse pour sa cuisse gauche, pour sa cuisse gauche seulement,

Car lui aussi boit un spritz à 8\ € et je trouve que ça n'a vraiment rien d'épique,

Ça raconte toujours quelque chose un tatouage, sur sa cuisse gauche il y a plein de tatouages,

Il y a du rouge il y a du noir sur sa cuisse gauche,

Je ferme les yeux et j'imagine alors des dragons noirs dansant sur sa cuisse,

Des dragons rouges volant au-dessus de nous sur le Cours Estienne d'Orves,

Et plein de fleurs sur sa cuisse, des fleurs dont je ne connais pas les noms, des fleurs qui se déploient, grandissent, déploient leurs pétales jusqu'aux ciels,

Et les ciels s'écartèlent pour laisser place à des soleils enchanteurs

Qui s'abattent comme des sentences sur cette grande Nuit des hommes,

Et tous les abrutis du Cours Estienne d'Orves renversent leur spritz à 8\ € par terre et se mettent à danser,

Et des soleils très jaunes étirent leurs toiles mystiques vers des teintes orangées,

C'est à ses côtés, je crois, que j'ai appris à aimer les ciels fauves qui s'écartèlent,

Et c'est ainsi que je suis né d'un baiser,

C'est une folie de croire que nous naissons de l'union biologique de deux êtres, c'est une folie de croire que nous sommes seuls,

Nous naissons toujours d'un baiser, nous naissons toujours sous des soleils orangés,

Mais depuis qu'elle m'a abandonné dans l'urine et la cocaïne d'un lundi soir d'automne

Tous les matins du monde dans mon far west

Ont le goût du côtes-du-rhône à 4,50\ € et de la défaite,

Tous les matins du monde sont pareils au chantier de requalification urbaine de la Place Jean Jaurès,

Et tous les hymnes à la nation et tous les hommages au grand Jacques et tous les flashs infos

Me donnent envie de mettre de la TNT dans mon cerveau et de me faire exploser,

Me donnent envie de chevaucher des dragons noirs, de chevaucher des dragons rouges,

Et de cracher du soufre et des couleurs épiques dans leur spritz à 8\ €,

Un spritz à 8\ €, ça n'est vraiment pas raisonnable,

Et tous les hymnes à la nation et tous les hommages au grand Jacques et tous les flashs infos

Je les diluerai dans ma gorge pleine de feu et de rancœur,

Et les ciels s'écartèleront encore sous l'impulsion de mes soleils rédempteurs,

Et nous bâtirons alors sur le Cours Estienne d'Orves une grande piscine municipale remplie de vins capiteux et de couleurs profondes et interdites,

Et les ciels écartelés chanteront,

Et les soleils orangés chanteront,

Et nos cerveaux remplis de TNT chanteront,

Et notre far west tout entier chantera,

Et nous aurons enfin l'art de fabriquer des rêves et des hurlements avec du pain sec et de l'eau,

Et tous ces rêves et tous ces hurlements nous serviront de fondations pour établir les conditions matérielles de la lumière et de la justice,

Et nous danserons ainsi avec tous les Gitans sétois,

Et nous chanterons alors avec tous les cocaïnomanes russes,

Et nous aurons droit à des festins raisonnables, composés de fruits et de légumes de saison,

Et nous jouerons de la clarinette Klezmer et des chansons profanes,

Et nous manipulerons à notre guise des ombres de prestige,

Valery, Éluard seront chargés de mettre le couvert et d'imiter le chant du coq,

Et nous réciterons des gammes anciennes en inversant les modes et les saisons,

Et nos soleils rouges nomades rouleront comme des ballons au bord de notre grande piscine,

Et nous jouerons alors des parties de football internationalistes, avec beaucoup de convictions esthétiques et sans aucun pragmatisme,

Et nous cracherons des ciels épileptiques avec nos gorges remplies de gouache et de douceur,

Et nous serons des peintres audacieux mais jamais des adultes,

Et nous travestirons les corps en leur épinglant des visages délicieusement cubiques,

Et nos caresses seront voraces et tendres comme un bambou de Chine,

Et notre far west sera sans limites,

Il épousera l'Histoire en la faisant danser dans les bras de la mémoire,

Et nous l'éclabousserons de miel éthiopien et de café noir,

Et nous tacherons savamment toutes ses lettres de noblesse,

Et tout sera gratuit et tout sera épique ­-- il n'y aura plus aucun touriste.

*

Je me suis retrouvé tout seul avec un cerveau rempli de TNT,

Je n'ai même pas réussi à me faire exploser,

Mais je suis parti sans payer,

Ce soir, c'est vraiment tout ce que j'ai gagné,

Le Cours Estienne d'Orves ce n'est pas bien pour les cœurs purs comme le mien,

J'enfourche à nouveau un TER à la Gare Saint-Charles,

Je préfère les TER aux Intercités car dans les Intercités il est plus difficile de frauder,

Ce sont des trains avec "réservations obligatoires", disent-ils de leur voix sans pitié,

J'éteins la radio et je regarde vers elle, vers les vallées cévenoles de son adolescence,

Je l'imagine à Sauve ou à Saint-Jean dégustant des pélardons bien crémeux avec du pain à la miche bien tendre,

Et mon TER s'élance,

À Alès, il y aura peut-être une Jeanne de France à sauver des flammes ou des inondations,

Alès, ce n'est pas vraiment beau, 15 % de chômage sans compter tous les contrats précaires,

Un drôle d'équateur sépare Alès de mes soleils enchanteurs,

Et pourtant, Alès, c'est déjà un peu la porte des Cévennes,

Une brume épaisse, une brume qui ne chante plus depuis 1980,

Si j'étais muni d'un dragon noir ou rouge je pourrais ouvrir une brèche de feu et de douceur dans cette brume épaisse,

Mais je ne peux rien du tout contre le chômage de masse,

Le néolibéralisme ressemble à une brume épaisse qui ne chante plus depuis 1980,

Les soleils enchanteurs, dans le ciel d'Alès, ont disparu depuis 1980,

--- Et ma Jeanne dans tout ça ?

C'est l'automne, c'est toujours l'automne à Alès,

Le néolibéralisme est un éternel automne,

J'ai postulé à un poste de serveur à Ouessant,

Ouessant c'est une île bretonne,

Et j'imagine alors les vagues bretonnes caresser la brume d'Alès,

Mais je suis trop diplômé, disent-ils, pour être serveur,

Pourtant, il me faut des vagues bretonnes pour caresser la brume d'Alès,

Et si je n'obtiens pas ce boulot je crierai si fort

Que toutes les étoiles de l'univers s'échoueront dans la boue,

Dans la boue d'Alès,

Car il n'y a aucune raison pour que les étoiles chantent quand il y a 15 % de chômage sans compter tous les contrats précaires,

Et tous les ciels écartelés et tous les soleils orangés peuvent bien chanter,

Toutes les étoiles de l'univers je les roulerai dans la boue,

Et tous les matins du monde sont une flaque de boue,

Alès est un désert recouvert de boue,

Et au *Lidl* d'Alès, les employées se flinguent régulièrement le dos,

On ne compte plus le nombre de hernies discales ces derniers mois,

C'est très difficile de faire reconnaître un accident de travail au *Lidl* d'Alès,

Les employées ont peur de perdre leur emploi,

*Lidl*, c'est la quintessence de la brume épaisse,

Pourvu qu'il ne pleuve pas avant Anduze,

Sous le pont du Gard, je connais un petit banc de sable où je serai au sec,

Dans mon duvet, je ne crains ni la saison des catastrophes ni le chômage de masse,

Mais on s'emmerde, bien souvent, dans mon far west,

Un TER, ça n'a pas la dégaine d'un Transsibérien,

En soulevant des monticules de déchets, qui sait ?

Les soleils rédempteurs, à Alès, on ne les voit pas,

La grâce est une affaire sérieuse je crois,

Elle dépend de conditions matérielles,

Elle dépend également de notre capacité à souffler sur les braises,

Les gilets jaunes ont toujours raison de souffler sur les braises,

La trace incertaine d'un rêve et d'un hurlement,

Le néolibéralisme, Macron, tout ça, c'est notre gangrène,

Si je retourne à Alès, moi aussi je soufflerai sur les braises,

Un détour sur la N113 pour saluer tous les vieux copains,

La police d'État frappe avec des armes LBD 40 et grenades,

24 gilets jaunes ont perdu un œil, Zineb Redouane est morte en fermant sa fenêtre,

C'était à Marseille, c'était une grenade lacrymogène,

On ne compte plus les mains arrachées et les blessures de guerre,

Nous nous efforcerons de souffler sur les braises, nous nous efforcerons d'y croire,

Et même si notre monture n'est qu'un vieux TER,

Et même si nos balles de revolver ne sont qu'un peu de sève coulant sur le tronc d'un mélèze,

Et même si Macron se fout éperdument de notre sève,

Et même s'il n'a jamais goûté à la chaleur de son épiderme,

Et même s'il ignore tout de nos soleils enchanteurs et de leurs éclats orangés,

Et même s'il n'a aucune appétence pour les ciels fauves qui s'écartèlent,

Nous nous efforcerons de souffler sur les braises, nous nous efforcerons d'y croire,

Nous nous efforcerons de ressusciter tous les ciels du monde au-dessus d'Alès,

Et tout en haut de notre beau mélèze, nous nous nourrirons de sève,

Et si je monte seul, les copains, ne vous inquiétez pas,

C'est afin de réchauffer toutes les chaumières d'Alès,

Mon haleine est si chaude, j'aurai l'art de réchauffer les chaumières, toutes les chaumières d'Alès,

Et je planterai mes yeux de gnôle et mes prières dans la brume épaisse et grise d'Alès,

Je t'aime tant ma Jeanne, je t'en prie, entends mes prières,

Je te prépare un petit lopin de terre et une cabane tout en haut de notre beau mélèze,

Un tout nouveau mirador pour guetter le couchant des oranges plénières,

Et tous ces ciels intestins et tous ces ciels lointains,

Les ciels bulgares, bien sûr,

Et ceux de cette petite ville au sud de Sofia dont tu m'as dit un jour qu'elle était celle des poètes,

Si j'oublie son nom, il ne faut pas que tu t'en offusques,

J'oublie toujours le nom des villes un peu trop lointaines,

J'ai une mémoire vive pour les choses que je touche, le reste m'importe peu,

J'aurai toujours besoin de toi pour apprendre à nommer les choses lointaines,

Et tous ces ciels verts et roses de ta bulgare insomnie,

Et tous les ciels andalous,

Nous referons Grenade, nous reprendrons Cordoue,

Nous nous glisserons dans les patios ombragés des petites maisons arabes du quartier d'Albaicín au pied du Sacromonte,

Nous aurons des passions simples, le goût de l'ail et du cumin, et cette bouillie au pain de mie qui nous a tant fascinés,

L'Alhambra à Alès, imagine un peu, tous les ciels d'Andalousie au-dessus d'Alès, imagine un peu la dégaine,

Non, ne riez pas les copains,

Laissez-moi inventer, pour ma Jeanne envolée,

Tout un ciel andalou au-dessus d'Alès,

Tous les ciels d'Andalousie, au-dessus d'Alès,

Car il n'y a rien du tout après l'amour et sans toi je suis seul au milieu des copains,

Je tire à boulets francs sur tous les rêves sur tous les miradors sur tous les monticules recouvrant le bassin minier d'Alès,

Sans toi je n'ai même plus le goût des ronds-points et de la N113, je n'ai même plus le goût des copains,

Et même l'Alhambra n'est plus qu'un amoncellement de gravats de monticules de peines,

Et à Grenade, on se souvient de la barbarie de l'Église chrétienne,

Et je me dis que si les Arabes avaient conquis toute l'Europe, il y aurait des patios ombragés partout, des orangers en enfilade, une musique suave et démocratique aux quatre coins du continent,

Et notre vie serait résolument plus douce, et la poésie serait un ART VIVANT,

Charles Martel est assurément de ceux qui boivent des spritz à 8\ € sur le cours Estienne d'Orves, il est urgent de l'en chasser,

Il est urgent de noyer Charles Martel dans notre belle piscine municipale remplie de vins capiteux et de couleurs profondes et interdites,

Et nous tairons alors nos yeux de gnôle et de rancœur,

Et nous assumerons ce monde qui nous est donné,

Et nous transformerons notre apocalypse en armée,

Tout ceci pour te dire, ma petite Jehanne de France, que le ciel pauvre et moribond d'Alès nous l'écartèlerons,

Tout ceci pour te dire, ma Jeanne, que toutes les brumes épaisses et grasses et grises d'Alès nous les soulèverons,

Et tout ceci, Jeanne, afin que lumière et grâce puissent se frayer un chemin jusqu'à nous,

Entends-les, Jeanne, tends l'oreille,

Tous les copains tous les dragons rouges et noirs,

Tous les Alhambra tous les patios ombragés toutes les enfilades d'orangers,

Toutes les musiques suaves et démocratiques toutes les douceurs,

Tous les ciels épileptiques tous les soleils nomades,

Toutes les vieilles C15 toutes blanches et toutes rouillées,

Toutes les gammes inversées toutes les prières toutes les lucarnes,

Toutes les brumes épaisses et grasses et grises tous les Blaise Cendrars,

Toutes les employées de chez *Lidl* tous les accidentés,

Tous les bassins miniers toutes les insomnies bulgares,

Tous les TER toutes les montures et tous les revolvers,

Tous les Gitans du monde toutes les clarinettes Klezmer,

Tous les gilets rouges et noirs tous les ronds-points occupés,

Toutes les gorges ulcérées tous les tambours battants,

Tous les rêves et tous les hurlements

    souffler sur les braises

          en éclairant,     déjà,

                  *les voies nouvelles*.
